using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace banco_futuro.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ElementRiskController : ControllerBase
    {
        private static readonly string[,] Statements = new string[,]
        {
            {
                "(Time to Market) demora en las entregas",
                "(Escalable) Ausencia de arquitectura escalable",
                "(Metricas) Ausencia de metricas de esfuerzo para el grupo de trabajo",
                "(Lineamientos) No seguir los lineamientos de la metodologia a abarcar"
            },
            {
                "(Acoplamiento) Bajo acoplamiento de las tecnologias a utilizar",
                "(Alto flujo de personal) Deserción del equipo de desarrollo",
                "(Baja Documentación) Baja documentación en los procesos",
                "(Seniority) Alta diferencia en la experiencia en el equipo de trabajo"
            },
            {
                "(Deuda tecnica) Aumento en la deuda tecnica pendiente",
                "(Baja comunicación) Baja comunicación o socialización con personal de negocio",
                "(Costos) Altos costos en infraestructura",
                "(Comprensión) baja comprensión de los requerimientos del cliente"
            }
        };
        private readonly ILogger<ElementRiskController> _logger;
        public ElementRiskController(ILogger<ElementRiskController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<ElementRisk[]> Get()
        {
            List<ElementRisk[]> elementRisks = new List<ElementRisk[]>();

            elementRisks.Add(new ElementRisk[]{
                    new ElementRisk("rojo",Statements[0,0]),
                    new ElementRisk("rojo",Statements[0,1]),
                    new ElementRisk("amarillo",Statements[0,2]),
                    new ElementRisk("amarillo",Statements[0,3]),
                });

            elementRisks.Add(new ElementRisk[]{
                    new ElementRisk("rojo",Statements[1,0]),
                    new ElementRisk("amarillo",Statements[1,1]),
                    new ElementRisk("amarillo",Statements[1,2]),
                    new ElementRisk("verde",Statements[1,3]),
                });

            elementRisks.Add(new ElementRisk[]{
                    new ElementRisk("amarillo",Statements[2,0]),
                    new ElementRisk("amarillo",Statements[2,1]),
                    new ElementRisk("verde",Statements[2,2]),
                    new ElementRisk("verde",Statements[2,3]),
                });

            return elementRisks;
        }
    }
}
