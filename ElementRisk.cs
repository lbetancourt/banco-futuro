using System;

namespace banco_futuro
{
    public class ElementRisk
    {
        public string color { get; set; }

        public string statement { get; set; }

        public ElementRisk(string color, string statement)
        {
            this.color = color;
            this.statement = statement;
        }
    }
}