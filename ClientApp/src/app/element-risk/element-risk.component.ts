import { Component, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-element-risk",
  templateUrl: "./element-risk.component.html",
  styleUrls: ['./element-risk.component.css']
})
export class ElementRiskComponent {
  public elementsrisks: ElementRisk[][];

  constructor(http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    http.get<ElementRisk[][]>(baseUrl + "elementrisk").subscribe(
      (result) => {
        this.elementsrisks = result;
        console.log(result);
      },
      (error) => console.error(error)
    );
  }
}

interface ElementRisk {
  color: string;
  statement: string;
}
